package com.example.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MaewListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maew_list)
        val number:String = intent.getStringExtra("number")?:"Unknow"
        val txtNumber = findViewById<TextView>(R.id.txtNumber)
        txtNumber.text = number
    }
}