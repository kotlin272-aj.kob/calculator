package com.example.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val txtNumber = findViewById<TextView>(R.id.txtNumber)
        val btnC = findViewById<Button>(R.id.btnC)
        val btn9 = findViewById<Button>(R.id.btn9)
        val btn8 = findViewById<Button>(R.id.btn8)
        val btn7 = findViewById<Button>(R.id.btn7)
        val btn6 = findViewById<Button>(R.id.btn6)
        val btn5 = findViewById<Button>(R.id.btn5)
        val btn4 = findViewById<Button>(R.id.btn4)
        val btn3 = findViewById<Button>(R.id.btn3)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn1 = findViewById<Button>(R.id.btn1)
        val btnEquals = findViewById<Button>(R.id.btnEquals)
        btnC.setOnClickListener{
            txtNumber.text = "0"
        }
        btnEquals.setOnClickListener{
            txtNumber.text = (btn1.text.toString().toInt() + btn2.text.toString().toInt()).toString()
        }
        btn9.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn9.text}"
        }
        btn8.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn8.text}"
        }
        btn7.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn7.text}"
        }
        btn6.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn6.text}"
        }
        btn5.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn5.text}"
        }
        btn4.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn4.text}"
        }
        btn3.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn3.text}"
        }
        btn2.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn2.text}"
        }
        btn1.setOnClickListener{
            txtNumber.text = "${txtNumber.text}${btn1.text}"
        }
        val btnMaewList = findViewById<Button>(R.id.btnMaewList)
        btnMaewList.setOnClickListener{
            val intent = Intent(MainActivity@this,MaewListActivity::class.java)
            intent.putExtra("number",txtNumber.text)
            startActivity(intent)
        }
    }
}